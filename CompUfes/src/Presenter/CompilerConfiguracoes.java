/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.ClassWithTokens;
import java.io.File;

/**
 *
 * @author Avell B154 PLUS
 */
public class CompilerConfiguracoes {

    public static void generateLexer(String path) {
        File file = new File(path);
        jflex.Main.generate(file);
    }

    public static void configurarCompilador() {
        generateLexer("src/compiler/Lexer.flex");
        ClassWithTokens tokens = ClassWithTokens.getClassWithTokens();
    }

}
