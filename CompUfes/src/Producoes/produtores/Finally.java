/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Finally extends Production {

    public Finally(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("FINALLY");
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No("FINALLY");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<block>");
            no.addNo(no_2);
            this.codigo = new Block(tokens).handle(tokens,no_2);
        } else {
            this.codigo = 24;
        }
        return this.codigo;
    }

}
