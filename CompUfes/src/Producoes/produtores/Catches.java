/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Catches extends Production {

    public Catches(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return new CatchClause(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<catchClause>");
        no.addNo(no_1);
        this.codigo = new CatchClause(tokens).handle(tokens, no_1);
        if (this.codigo == 0) {
            Catches ct = new Catches(tokens);
            if (ct.accept(tokens.getFirstToken())) {
                No no_2 = new No("<catches>");
                no.addNo(no_2);
                this.codigo = ct.handle(tokens, no_2);
            }
        }
        return this.codigo;
    }

}
