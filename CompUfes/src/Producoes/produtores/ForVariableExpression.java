/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ForVariableExpression extends Production {

    public ForVariableExpression(ClassWithTokens tokens) {
        super(tokens);
    }

    @Override
    public boolean accept(String token) {
        return (new Type(tokens).accept(token) || new IdentifierProduction1(tokens).accept(token));
    }

    @Override
    public int nextProduction(No no) {
        Type type = new Type(tokens);
        if (type.accept(tokens.getFirstToken())) {
            No no_1 = new No("<type>");
            no.addNo(no_1);
            this.codigo = type.handle(tokens, no_1);
            if (this.codigo == 0) {
                No no_2 = new No("<identifier>");
                no.addNo(no_2);
                this.codigo = new IdentifierProduction(tokens).handle(tokens, no_2);
                if (this.codigo == 0) {
                    No no_3 = new No("<variableDeclarators>");
                    no.addNo(no_3);
                    this.codigo = new VariableDeclarators(tokens).handle(tokens, no_3);
                }
            }
        } else {
            No no_4 = new No("<identifier>");
            no.addNo(no_4);
            this.codigo = new IdentifierProduction1(tokens).handle(tokens, no_4);
            if (this.codigo == 0) {
                No no_5 = new No("<variableDeclarator>");
                no.addNo(no_5);
                this.codigo = new VariableDeclarator(tokens).handle(tokens, no_5);
            }
        }

        return this.codigo;
    }

}
