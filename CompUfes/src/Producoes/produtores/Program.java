/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Program extends Production {

    public Program(ClassWithTokens tokens) {
        super(tokens);
    }

    @Override
    public int nextProduction(No no) {
        PackageDeclaration pd = new PackageDeclaration(tokens);
        ImportDeclarations id = new ImportDeclarations(tokens);
        ClassDeclaration cd = new ClassDeclaration(tokens);
        String token = tokens.getFirstToken();

        if ((pd.accept(token) || id.accept(token) || cd.accept(token))){
            if (pd.accept(token)) {
                No novoNo = new No("<packageDeclaration>");
                no.addNo(novoNo);
                this.codigo = pd.handle(tokens,novoNo);
            }
            token = tokens.getFirstToken();
            if (id.accept(token) && this.codigo == 0) {
                No novoNo = new No("<importDeclarations>");
                no.addNo(novoNo);
                this.codigo = id.handle(tokens,novoNo);
            }
            token = tokens.getFirstToken();
            if (cd.accept(token) && this.codigo == 0) {
                No novoNo = new No("<classDeclaration>");
                no.addNo(novoNo);
                this.codigo = cd.handle(tokens,novoNo);
            }
        } else if(!tokens.isEmpity()) {
            this.codigo = 34;
        }
        return this.codigo;
    }

}
