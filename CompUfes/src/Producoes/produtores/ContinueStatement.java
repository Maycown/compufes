/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ContinueStatement extends Production {

    public ContinueStatement(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("CONTINUE");
        nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("CONTINUE");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            IdentifierProduction i = new IdentifierProduction(tokens);
            if (i.accept(token)) {
                No no_2 = new No("<identifier>");
                no.addNo(no_2);
                this.codigo = i.handle(tokens, no_2);
            }
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (nomes.get(1).equals(token)) {
                    No no_3 = new No(";");
                    no.addNo(no_3);
                    tokens.removeFirst();
                } else {
                    this.codigo = 1;
                }
            }
        } else {
            this.codigo = 20;
        }

        return this.codigo;
    }

}
