/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class MethodInvocation extends Production {

    public MethodInvocation(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("LEFTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
        nomes.add("SUPER");
        nomes.add("DOT");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token) || nomes.get(2).equals(token);
    }

    private int argumentListProduction(String token, No no) {
        int codigo = 0;
        ArgumentList al = new ArgumentList(tokens);
        if (al.accept(token)) {
            No no_1 = new No("<argumentList>");
            no.addNo(no_1);
            codigo = al.handle(tokens, no_1);
        }
        return codigo;
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("(");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            this.codigo = this.argumentListProduction(token, no);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (nomes.get(1).equals(token)) {
                    No no_2 = new No(")");
                    no.addNo(no_2);
                    tokens.removeFirst();
                } else {
                    this.codigo = 12;
                }
            }
        } else if (nomes.get(2).equals(token)) {
            No no_1 = new No("SUPER");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if (nomes.get(3).equals(tokens)) {
                No no_2 = new No("DOT");
                no.addNo(no_2);
                tokens.removeFirst();
                token = tokens.getFirstToken();
                if (nomes.get(0).equals(token)) {
                    No no_3 = new No("(");
                    no.addNo(no_3);
                    tokens.removeFirst();
                    token = tokens.getFirstToken();
                    this.codigo = this.argumentListProduction(token, no);
                    if (this.codigo == 0) {
                        token = tokens.getFirstToken();
                        if (nomes.get(1).equals(token)) {
                            No no_5 = new No(")");
                            no.addNo(no_5);
                            tokens.removeFirst();
                        } else {
                            this.codigo = 12;
                        }
                    }
                } else {
                    this.codigo = 11;
                }
            } else {
                this.codigo = 31;
            }

        } else {
            this.codigo = 11;
        }

        return this.codigo;
    }

}
