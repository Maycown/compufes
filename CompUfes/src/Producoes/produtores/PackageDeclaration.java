/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class PackageDeclaration extends Production {

    public PackageDeclaration(ClassWithTokens tokens) {
        super(tokens);
        codigo = 101;
        nomes.add("PACKAGE");
        nomes.add("SEMICOLON");
    }
    
    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("PACKAGE");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<identifier>");
            this.codigo = new IdentifierProduction(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                no.addNo(no_2);
                token = tokens.getFirstToken();
                if (nomes.get(1).equals(token)) {
                    No no_3 = new No(";");
                    no.addNo(no_3);
                    tokens.removeFirst();
                } else {
                    this.codigo = 1;
                }
            }
        }
        return this.codigo;
    }
}
