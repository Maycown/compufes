/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Egle
 */
public class ImportDeclaration extends Production {

    public ImportDeclaration(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("IMPORT");
        nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No("IMPORT");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<identifier>");
            no.addNo(no_2);
            this.codigo = new IdentifierProduction(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (nomes.contains(token)) {
                    No no_3 = new No(";");
                    no.addNo(no_3);
                    tokens.removeFirst();
                } else {
                    this.codigo = 1;
                }
            }
        }

        return this.codigo;
    }

}
