/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class Statement extends Production {

    public Statement(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return new Block(tokens).accept(token) || new EmptyStatement(tokens).accept(token)
                || new IdentifierProduction(tokens).accept(token) || new DoStatement(tokens).accept(token)
                || new BreakStatement(tokens).accept(token) || new ContinueStatement(tokens).accept(token)
                || new ReturnStatement(tokens).accept(token) || new IfSstatement(tokens).accept(token)
                || new WhileStatement(tokens).accept(token) || new ForDeclaration(tokens).accept(token)
                || new TryStatement(tokens).accept(token) || new ClassInstanceCreationExpression(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        Block b = new Block(tokens);
        EmptyStatement es = new EmptyStatement(tokens);
        Type tp = new Type(tokens);
        IdentifierProduction i = new IdentifierProduction(tokens);
        DoStatement ds = new DoStatement(tokens);
        BreakStatement bs = new BreakStatement(tokens);
        ContinueStatement cs = new ContinueStatement(tokens);
        ReturnStatement rs = new ReturnStatement(tokens);
        IfSstatement is = new IfSstatement(tokens);
        WhileStatement ws = new WhileStatement(tokens);
        TryStatement ts = new TryStatement(tokens);
        ClassInstanceCreationExpression cice = new ClassInstanceCreationExpression(tokens);
        ForDeclaration f = new ForDeclaration(tokens);
        String token = tokens.getFirstToken();

        if (b.accept(token)) {
            No no_1 = new No("<block>");
            no.addNo(no_1);
            this.codigo = b.handle(tokens, no_1);
        } else if (es.accept(token)) {
            No no_1 = new No("<emptyState>");
            no.addNo(no_1);
            this.codigo = es.handle(tokens, no_1);
        } else if (i.accept(token)) {
            No no_1 = new No("<identifier>");
            no.addNo(no_1);
            this.codigo = i.handle(tokens, no_1);
            if (this.codigo == 0) {
                No no_2 = new No("<statementExpression>");
                no.addNo(no_2);
                this.codigo = new StatementExpression(tokens).handle(tokens, no_2);
                if (this.codigo == 0) {
                    token = tokens.getFirstToken();
                    if (nomes.contains(token)) {
                        No no_3 = new No(";");
                        no.addNo(no_3);
                        tokens.removeFirst();
                    } else {
                        this.codigo = 1;
                    }
                }
            }
        } else if (ds.accept(token)) {
            No no_1 = new No("<doStatement>");
            no.addNo(no_1);
            this.codigo = ds.handle(tokens, no_1);
        } else if (bs.accept(token)) {
            No no_1 = new No("<breakStatement>");
            no.addNo(no_1);
            this.codigo = bs.handle(tokens, no_1);
        } else if (cs.accept(token)) {
            No no_1 = new No("<continueStatement>");
            no.addNo(no_1);
            this.codigo = cs.handle(tokens, no_1);
        } else if (rs.accept(token)) {
            No no_1 = new No("<returnStatement>");
            no.addNo(no_1);
            this.codigo = rs.handle(tokens, no_1);
        } else if (is.accept(token)) {
            No no_1 = new No("<ifStatement>");
            no.addNo(no_1);
            this.codigo = is.handle(tokens, no_1);
        } else if (ws.accept(token)) {
            No no_1 = new No("<whileStatement>");
            no.addNo(no_1);
            this.codigo = ws.handle(tokens, no_1);
        } else if (ts.accept(token)) {
            No no_1 = new No("<tryStatement>");
            no.addNo(no_1);
            this.codigo = ts.handle(tokens, no_1);
        } else if (cice.accept(token)) {
            No no_1 = new No("<classInstanceCreationExpression>");
            no.addNo(no_1);
            this.codigo = cice.handle(tokens, no_1);
        } else if (f.accept(token)) {
            No no_1 = new No("<forDeclaration>");
            no.addNo(no_1);
            this.codigo = f.handle(tokens, no_1);
        } else {
            this.codigo = 1002;
        }

        return this.codigo;

    }

}
