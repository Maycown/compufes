/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class StatementExpression extends Production{

    public StatementExpression(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 1003;
    }

    @Override
    public int nextProduction(No no) {
        Assignment as = new Assignment(tokens);
        MethodInvocation mi = new MethodInvocation(tokens);
        LabeledStatement ls = new LabeledStatement(tokens);
        String token = tokens.getFirstToken();
        if(as.accept(token)){
            No no_1 = new No("<assignment>");
            no.addNo(no_1);
            this.codigo = as.handle(tokens,no_1);
        } else if(mi.accept(token)){
            No no_2 = new No("<methodInvocation>");
            no.addNo(no_2);
            this.codigo = mi.handle(tokens,no_2);
        } else if(ls.accept(token)){
            No no_3 = new No("<labledStatement>");
            no.addNo(no_3);
            this.codigo = ls.handle(tokens,no_3);
        }
        return this.codigo;             
    }
    
}
