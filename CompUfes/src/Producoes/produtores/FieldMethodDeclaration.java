/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class FieldMethodDeclaration extends Production {

    public FieldMethodDeclaration(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return new FieldDeclaration(tokens).accept(token) || new MethodDeclaration(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        MethodDeclaration md = new MethodDeclaration(tokens);
        if (md.accept(token)) {
            No no_1 = new No("<methodDeclaration>");
            no.addNo(no_1);
            this.codigo = md.handle(tokens, no_1);
        } else {
            No no_1 = new No("<fieldDeclaration>");
            no.addNo(no_1);
            this.codigo = new FieldDeclaration(tokens).handle(tokens, no_1);
        }
        return this.codigo;
    }
}
