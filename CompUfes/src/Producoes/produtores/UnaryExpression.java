/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class UnaryExpression extends Production {

    public UnaryExpression(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("SUM");
        nomes.add("MINUS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.contains(token) || (new IdentifierProduction1(tokens).accept(token));
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No(tokens.getSecondToken());
            no.addNo(no_1);
            tokens.removeFirst();
        }
        No no_2 = new No("<identifier>");
        no.addNo(no_2);
        this.codigo = new IdentifierProduction1(tokens).handle(tokens, no_2);
        if (this.codigo == 0) {
            token = tokens.getFirstToken();
            MethodInvocation mi = new MethodInvocation(tokens);
            if (mi.accept(token)) {
                No no_3 = new No("<methodInvocation>");
                no.addNo(no_3);
                this.codigo = mi.handle(tokens, no_3);
            }
        }

        return this.codigo;
    }

}
