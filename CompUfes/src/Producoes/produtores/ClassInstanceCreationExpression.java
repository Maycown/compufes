/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ClassInstanceCreationExpression extends Production {

    public ClassInstanceCreationExpression(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("NEW");
        nomes.add("LEFTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("NEW");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<identfier>");
            no.addNo(no_2);
            this.codigo = new IdentifierProduction(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (nomes.get(1).equals(token)) {
                    No no_3 = new No("(");
                    no.addNo(no_3);
                    tokens.removeFirst();
                    ArgumentList al = new ArgumentList(tokens);
                    if (al.accept(token)) {
                        No no_4 = new No("<argumentList>");
                        no.addNo(no_4);
                        this.codigo = al.handle(tokens, no_4);
                    }
                    if (this.codigo == 0) {
                        token = tokens.getFirstToken();
                        if (nomes.get(2).equals(token)) {
                            No no_5 = new No(")");
                            no.addNo(no_5);
                            tokens.removeFirst();
                        } else {
                            this.codigo = 12;
                        }
                    }
                } else {
                    this.codigo = 11;
                }

            }
        } else {
            this.codigo = 32;
        }
        return this.codigo;
    }
}
