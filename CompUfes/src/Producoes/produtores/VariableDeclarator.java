/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class VariableDeclarator extends Production {

    public VariableDeclarator(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("ASSIGN");
    }

    @Override
    public boolean accept(String token) {
        return true;
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No("=");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<expression>");
            no.addNo(no_2);
            this.codigo = new Expression(tokens).handle(tokens, no_2);
        }
        return this.codigo;
    }
}
