/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class MethodDeclaration extends Production {

    public MethodDeclaration(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return new MethodDeclarator(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<methodDeclarator>");
        no.addNo(no_1);
        this.codigo = new MethodDeclarator(tokens).handle(tokens, no_1);
        if (this.codigo == 0) {
            No no_2 = new No("<methodBody>");
            no.addNo(no_2);
            this.codigo = new MethodBody(tokens).handle(tokens, no_2);
        }
        return this.codigo;
    }

}
