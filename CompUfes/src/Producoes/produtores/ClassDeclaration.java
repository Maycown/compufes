/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ClassDeclaration extends Production {

    public ClassDeclaration(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("CLASS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token) || new ClassModifier(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        ClassModifier cm = new ClassModifier(tokens);
        if (cm.accept(token)) {
            No no_1 = new No("<classModfier>");
            no.addNo(no_1);
            this.codigo = cm.handle(tokens, no_1);
        }

        if (this.codigo == 0) {
            token = tokens.getFirstToken();
            if (nomes.contains(token)) {
                No no_1 = new No("CLASS");
                no.addNo(no_1);
                tokens.removeFirst();
                No no_2 = new No("<identifier>");
                no.addNo(no_2);
                this.codigo = new IdentifierProduction(tokens).handle(tokens, no_2);
                if (this.codigo == 0) {
                    No no_3 = new No("<classBody>");
                    no.addNo(no_3);
                    this.codigo = new ClassBody(tokens).handle(tokens, no_3);
                }
            } else {
                this.codigo = 34;
            }
        }
        return this.codigo;
    }
}
