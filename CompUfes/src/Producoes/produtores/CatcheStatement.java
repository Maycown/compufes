/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class CatcheStatement extends Production {

    public CatcheStatement(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        Finally f = new Finally(tokens);
        if (f.accept(token)) {
            No no_1 = new No("<finally>");
            no.addNo(no_1);
            this.codigo = f.handle(tokens, no_1);
        } else {
            No no_1 = new No("<catches>");
            no.addNo(no_1);
            this.codigo = new Catches(tokens).handle(tokens, no_1);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (f.accept(token)) {
                    No no_2 = new No("<finally>");
                    no.addNo(no_2);
                    this.codigo = f.handle(tokens, no_2);
                }
            }
        }

        return this.codigo;
    }

}
