/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class TryStatement extends Production {

    public TryStatement(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("TRY");
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No("TRY");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            No no_2 = new No("<block>");
            no.addNo(no_2);
            this.codigo = new Block(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                No no_3 = new No("<catcheStatement>");
                no.addNo(no_3);
                this.codigo = new CatcheStatement(tokens).handle(tokens, no_3);
            }

        } else {
            this.codigo = 22;
        }
        return this.codigo;
    }

}
