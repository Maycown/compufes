/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ClassBodyDeclaration extends Production {

    public ClassBodyDeclaration(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return new Modifier(tokens).accept(token) || new Type(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        Modifier modificador = new Modifier(tokens);
        if (modificador.accept(tokens.getFirstToken())) {
            No no_1 = new No("<modifier>");
            no.addNo(no_1);
            this.codigo = modificador.handle(tokens, no_1);
        }
        if (this.codigo == 0) {
            No no_1 = new No("<type>");
            no.addNo(no_1);
            this.codigo = new Type(tokens).handle(tokens, no_1);
            if (this.codigo == 0) {
                No no_2 = new No("<identifierProduction>");
                no.addNo(no_2);
                this.codigo = new IdentifierProduction(tokens).handle(tokens, no_2);
                if (this.codigo == 0) {
                    No no_3 = new No("<fieldMethodDeclaration>");
                    no.addNo(no_3);
                    this.codigo = new FieldMethodDeclaration(tokens).handle(tokens, no_3);
                }
            }
        }

        return this.codigo;
    }
}
