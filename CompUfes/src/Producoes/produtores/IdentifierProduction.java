/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Avell B154 PLUS
 */
public class IdentifierProduction extends Production {

    public IdentifierProduction(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("ID");
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No(tokens.getSecondToken());
            no.addNo(no_1);
            tokens.removeFirst();
        } else {
            this.codigo = 1001;
        }
        return this.codigo;
    }
}
