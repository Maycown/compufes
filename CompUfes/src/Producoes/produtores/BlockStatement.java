/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class BlockStatement extends Production {

    public BlockStatement(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return new LocalVariableDeclaration(tokens).accept(token) || new Statement(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        LocalVariableDeclaration lvd = new LocalVariableDeclaration(tokens);
        if (lvd.accept(token)) {
            No no_1 = new No("<localVariableDeclaration>");
            no.addNo(no_1);
            this.codigo = lvd.handle(tokens, no_1);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (nomes.contains(token)) {
                    No no_2 = new No(";");
                    no.addNo(no_2);
                    tokens.removeFirst();
                } else {
                    this.codigo = 1;
                }
            }
        } else {
            No no_1 = new No("<Statement>");
            no.addNo(no_1);
            this.codigo = new Statement(tokens).handle(tokens, no_1);
        }

        return this.codigo;
    }

}
