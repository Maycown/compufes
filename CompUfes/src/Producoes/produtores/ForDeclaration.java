/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ForDeclaration extends Production {

    public ForDeclaration(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("FOR");
        nomes.add("LEFTPARENTESIS");
        nomes.add("SEMICOLON");
        nomes.add("RIGHTPARENTESIS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("FOR");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if (nomes.get(1).equals(token)) {
                No no_2 = new No("(");
                no.addNo(no_2);
                tokens.removeFirst();
                token = tokens.getFirstToken();
                ForVariableExpression fve = new ForVariableExpression(tokens);
                if (fve.accept(token)) {
                    No no_3 = new No("<forVariableExpression>");
                    no.addNo(no_3);
                    this.codigo = fve.handle(tokens, no_3);
                }
                token = tokens.getFirstToken();
                if (nomes.get(2).equals(token)) {
                    if (this.codigo == 0) {
                        No no_4 = new No(";");
                        no.addNo(no_4);
                        tokens.removeFirst();
                        token = tokens.getFirstToken();
                        Expression exp_1 = new Expression(tokens);
                        if (exp_1.accept(token)) {
                            No no_5 = new No("<expression>");
                            no.addNo(no_5);
                            this.codigo = exp_1.handle(tokens, no_5);
                        }
                        token = tokens.getFirstToken();
                        if (nomes.get(2).equals(token)) {
                            if (this.codigo == 0) {
                                No no_6 = new No(";");
                                no.addNo(no_6);
                                tokens.removeFirst();
                                token = tokens.getFirstToken();
                                ForStatement fs = new ForStatement(tokens);
                                if (fs.accept(token)) {
                                    No no_7 = new No("<forStatement>");
                                    no.addNo(no_7);
                                    this.codigo = fs.handle(tokens, no_7);
                                }
                                token = tokens.getFirstToken();
                                if (this.codigo == 0) {
                                    if (nomes.get(3).equals(token)) {
                                        No no_8 = new No(")");
                                        no.addNo(no_8);
                                        tokens.removeFirst();
                                        No no_9 = new No("<methodBody>");
                                        no.addNo(no_9);
                                        this.codigo = new MethodBody(tokens).handle(tokens, no_9);
                                    } else {
                                        this.codigo = 12;
                                    }
                                }
                            }
                        } else {
                            this.codigo = 1;
                        }
                    }
                } else {
                    this.codigo = 1;
                }
            } else {
                this.codigo = 11;
            }
        } else {
            this.codigo = 33;
        }

        return this.codigo;
    }

}
