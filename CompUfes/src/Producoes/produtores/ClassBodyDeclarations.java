/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ClassBodyDeclarations extends Production {

    public ClassBodyDeclarations(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }

    @Override
    public boolean accept(String token) {
        return new ClassBodyDeclaration(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<classBodyDeclaration>");
        no.addNo(no_1);
        this.codigo = new ClassBodyDeclaration(tokens).handle(tokens, no_1);
        if (this.codigo == 0) {
            String token = tokens.getFirstToken();
            ClassBodyDeclaration cd = new ClassBodyDeclaration(tokens);
            while (cd.accept(token) && this.codigo == 0) {
                this.codigo = cd.handle(tokens, no);
                No no_2 = new No("<classBodyDeclaration>");
                no.addNo(no_2);
                token = tokens.getFirstToken();
            }
        }
        return this.codigo;
    }
}
