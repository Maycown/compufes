/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class VariableDeclarators extends Production {

    public VariableDeclarators(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("COMMA");
    }

    @Override
    public boolean accept(String token) {
        return new VariableDeclarator(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<variableDeclarator>");
        no.addNo(no_1);
        this.codigo = new VariableDeclarator(tokens).handle(tokens, no_1);
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_2 = new No(",");
            no.addNo(no_2);
            tokens.removeFirst();
            No no_3 = new No("<identifierProduction>");
            no.addNo(no_3);
            this.codigo = new IdentifierProduction(tokens).handle(tokens, no_3);
            if (this.codigo == 0) {
                No no_4 = new No("<variableDeclarators>");
                no.addNo(no_4);
                this.codigo = new VariableDeclarators(tokens).handle(tokens, no_4);
            }
        }
        return this.codigo;
    }

}
