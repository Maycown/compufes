/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class ImportDeclarations extends Production {

    public ImportDeclarations(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
    }
    
    @Override
    public boolean accept(String token) {
        return new ImportDeclaration(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<importDeclaration>"); 
        no.addNo(no_1);
        this.codigo = new ImportDeclaration(tokens).handle(tokens,no_1);
        if (this.codigo == 0 && !tokens.isEmpity()) {
            String token = tokens.getFirstToken();
            ImportDeclaration id = new ImportDeclaration(tokens);
            while (id.accept(token)) {
                No no_2 = new No("<importDeclaration>"); 
                no.addNo(no_2);
                this.codigo = new ImportDeclaration(tokens).handle(tokens,no_2);
                token = tokens.getFirstToken();
            }
        }
        return codigo;
    }
}
