/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class MultiplicativeExpression extends Production {

    public MultiplicativeExpression(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("MULT");
        nomes.add("DIV");
        nomes.add("MOD");
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.contains(token)) {
            No no_1 = new No(tokens.getSecondToken());
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<unaryExpression>");
            no.addNo(no_2);
            this.codigo = new UnaryExpression(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                No no_3 = new No("<multiplicativeExpresion>");
                no.addNo(no_3);
                this.codigo = new MultiplicativeExpression(tokens).handle(tokens, no_3);
            }
        }
        return this.codigo;
    }
}
