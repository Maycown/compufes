/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class MethodDeclarator extends Production {

    public MethodDeclarator(ClassWithTokens tokens) {
        super(tokens);
        codigo = 0;
        nomes.add("LEFTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();

        if (nomes.get(0).equals(token)) {
            No no_1 = new No("(");
            no.addNo(no_1);
            tokens.removeFirst();
            FormalParameterList cd = new FormalParameterList(tokens);
            token = tokens.getFirstToken();
            if (cd.accept(token)) {
                No no_2 = new No("<formalParameterList>");
                no.addNo(no_2);
                this.codigo = cd.handle(tokens, no_2);
            }
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (nomes.get(1).equals(token)) {
                    No no_3 = new No(")");
                    no.addNo(no_3);
                    tokens.removeFirst();
                } else {
                    this.codigo = 12;
                }
            }
        } else {
            this.codigo = 11;
        }
        return this.codigo;
    }
}
