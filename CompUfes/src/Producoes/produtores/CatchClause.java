/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class CatchClause extends Production {

    public CatchClause(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("CATCH");
        nomes.add("LEFTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("CATCH");
            no.addNo(no_1);
            tokens.removeFirst();
            token = tokens.getFirstToken();
            if (nomes.get(1).equals(token)) {
                No no_2 = new No("(");
                no.addNo(no_2);
                tokens.removeFirst();
                No no_3 = new No("<formalParameter>");
                no.addNo(no_3);
                this.codigo = new FormalParameter(tokens).handle(tokens, no_3);
                if (this.codigo == 0) {
                    token = tokens.getFirstToken();
                    if (nomes.get(2).equals(token)) {
                        No no_4 = new No(")");
                        no.addNo(no_4);
                        tokens.removeFirst();
                        No no_5 = new No("<block>");
                        no.addNo(no_5);
                        this.codigo = new Block(tokens).handle(tokens, no_5);
                    } else {
                        this.codigo = 12;
                    }
                }
            } else {
                this.codigo = 11;
            }
        } else {
            this.codigo = 23;
        }

        return this.codigo;
    }

}
