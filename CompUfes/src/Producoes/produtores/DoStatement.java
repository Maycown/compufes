/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class DoStatement extends Production {

    public DoStatement(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("DO");
        nomes.add("WHILE");
        nomes.add("LEFTTPARENTESIS");
        nomes.add("RIGHTPARENTESIS");
        nomes.add("SEMICOLON");
    }

    @Override
    public boolean accept(String token) {
        return nomes.get(0).equals(token);
    }

    @Override
    public int nextProduction(No no) {
        String token = tokens.getFirstToken();
        if (nomes.get(0).equals(token)) {
            No no_1 = new No("DO");
            no.addNo(no_1);
            tokens.removeFirst();
            No no_2 = new No("<statement>");
            no.addNo(no_2);
            this.codigo = new Statement(tokens).handle(tokens, no_2);
            if (this.codigo == 0) {
                token = tokens.getFirstToken();
                if (nomes.get(1).equals(token)) {
                    No no_3 = new No("WHILE");
                    no.addNo(no_3);
                    tokens.removeFirst();
                    token = tokens.getFirstToken();
                    if (nomes.get(2).equals(token)) {
                        No no_4 = new No("(");
                        no.addNo(no_4);
                        tokens.removeFirst();
                        No no_5 = new No("<expression>");
                        no.addNo(no_5);
                        this.codigo = new Expression(tokens).handle(tokens, no_5);
                        if (this.codigo == 0) {
                            token = tokens.getFirstToken();
                            if (nomes.get(3).equals(token)) {
                                No no_6 = new No(")");
                                no.addNo(no_6);
                                tokens.removeFirst();
                                token = tokens.getFirstToken();
                                if (nomes.get(3).equals(token)) {
                                    No no_7 = new No(";");
                                    no.addNo(no_7);
                                    tokens.removeFirst();
                                } else {
                                    this.codigo = 1;
                                }
                            } else {
                                this.codigo = 12;
                            }
                        }
                    } else {
                        this.codigo = 11;
                    }
                } else {
                    this.codigo = 17;
                }
            }
        } else {
            this.codigo = 18;

        }
        return this.codigo;
    }
}
