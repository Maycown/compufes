/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Producoes.produtores;

import Arvore.No;
import Collection.ClassWithTokens;
import Producoes.Production;

/**
 *
 * @author Matheus
 */
public class FormalParameterList extends Production {

    public FormalParameterList(ClassWithTokens tokens) {
        super(tokens);
        this.codigo = 0;
        nomes.add("COMMA");
    }

    @Override
    public boolean accept(String token) {
        return new FormalParameter(tokens).accept(token);
    }

    @Override
    public int nextProduction(No no) {
        No no_1 = new No("<formalParameter>");
        no.addNo(no_1);
        this.codigo = new FormalParameter(tokens).handle(tokens, no_1);
        if (this.codigo == 0) {
            String token = tokens.getFirstToken();
            if (nomes.contains(token)) {
                No no_2 = new No(",");
                no.addNo(no_2);
                tokens.removeFirst();
                No no_3 = new No("<formalParameterList>");
                no.addNo(no_3);
                this.codigo = new FormalParameterList(tokens).handle(tokens, no_3);
            }
        }
        return this.codigo;
    }
}
